package PROP1;
import robocode.HitByBulletEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
//import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * TorreRobot - a robot by (your name here)
 */
public class TorreRobot extends Robot
{
	/**
	 * run: TorreRobot's default behavior
	 */
	private static double bearingThreshold = 5;
	
	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar

		// Robot main loop
		turnLeft(getHeading());
		while(true) {
			// Replace the next 4 lines with any behavior you would like
			turnGunLeft(90);
			turnRadarLeft(90);
		}
	}
	
	double normalizeBearing(double bearing){
		while (bearing > 180) bearing -= 360;
		while (bearing < -180) bearing += 360;
		return bearing;
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		if (normalizeBearing(e.getBearing()) < bearingThreshold){
			fire(1);
		}
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		//turnLeft(180);
	}	
}
