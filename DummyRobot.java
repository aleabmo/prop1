package PROP1;
import robocode.HitByBulletEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
//import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * DummyRobot - a robot by (your name here)
 */
public class DummyRobot extends Robot
{
	public void run() {
		turnLeft(getHeading());
		while(true) {
			ahead(500);
			turnRight(90);
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		fire(1);
	}

	public void onHitByBullet(HitByBulletEvent e) {
		//turnLeft(180);
	}
}
