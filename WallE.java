package PROP1;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;

import robocode.AdvancedRobot;
import robocode.BulletHitEvent;
import robocode.CustomEvent;
import robocode.HitRobotEvent;
import robocode.RobotDeathEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import robocode.Condition;


/**
* @author Aleix Abengochea Molar 47762884A
* @author Patricia Ortega Granados 47196741M
*/
public class WallE extends AdvancedRobot {

	//Variable que anirem cambiant, per tal de definir direccio
  private byte moveDirection = 1;

  //Variable que emmagatzema la energia anterior.
  static double prevEnergy = 100.0;

	//Definim quina sera la potencia de la bala.
	static final double POTENCIA = 3.0;

	//Definim els tancs.
	Tanc robot, target;
	//Definim la llista de canons imaginaris.
	ArrayList<Cano> canons;

	//Definim la llista on guardarem totes les bales
	ArrayList<Bala> bales = new ArrayList<Bala>();
	
	//Aqui definirem els targets que tenim.
	HashMap<String, Tanc> tancs = new HashMap<String, Tanc>();

	//Mapa de joc
	Rectangle2D.Double mapa;
	Rectangle2D.Double mapaParets;

    /**
     * Mètode principal que es crida a cada tick del joc.
     */
	public void run(){
		//Deixem que cada part del robot es mogui de manera lliure.
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
		setAdjustRadarForRobotTurn(true);

		//Definim un rectangle igual al camp de joc.
		//Es interesant utilitzar la llibreria de java per tal de poder utilitzar algunes funcions utils.
		mapa = new Rectangle2D.Double(0,0,getBattleFieldWidth(), getBattleFieldHeight());
		mapaParets = new Rectangle2D.Double(30,30,getBattleFieldWidth()-60, getBattleFieldHeight()-60);

		//Instanciem tots els canons a implementar.
		canons = new ArrayList<Cano>();
		Cano canoInicial = new CanoEstatic();
		canoInicial.hits = 1;
		canons.add(canoInicial);
		canons.add(new CanoMoviment());
		canons.add(new CanoAsin());
		canons.add(new CanoEsquiva());

		while(true){
			robot = new Tanc();
			robot.name = getName();
			robot.velocity = getVelocity();
			robot.heading = getHeadingRadians();
			robot.x = getX();
			robot.y = getY();

			//Turn our radar
			if(target == null) {
				
				setTurnRadarRightRadians(Math.toRadians(45));
			
			} else {
				double radarTurnAngle = Utils.normalRelativeAngle(robot.absoluteAngleTo(target) - getRadarHeadingRadians());
				if(radarTurnAngle > 0)
					radarTurnAngle += .4;
				else
					radarTurnAngle -= .4;
				setTurnRadarRightRadians(radarTurnAngle);

			//Aqui ens dediquem a calcular la nova posicio de les bales.
			//Tambe comprovem si en teoria li ha donat o no a un target, i shi les vales han sortit del camp.
			ArrayList<Bala> toRemove = new ArrayList<Bala>();
			for(Bala bullet : bales) {
				//Movem la bala
				bullet.moureBala();
				//Comprovem si la bala li dona al target
				if(bullet.distance(target) < 25){

					//Incrementem el rating d'aquest cano ja que hem acertat.
					bullet.canoUtilitzat.hits += 1;
					toRemove.add(bullet);
				//Comprovem si la bala surt del mapa
				} else if(!mapa.contains(bullet)){
					toRemove.add(bullet);
				}
			}
			
			//Eliminem les bales que ja no estan al joc
			bales.removeAll(toRemove);
			
			//Si tenim un target disparem
			disparar();
			}

			//Activem el events si ens trobem a 30 px de les parets
			if(mapaParets.contains(robot)) {	
				//Event per tal d'evitar parets
				addCustomEvent(new Condition("hulio") {
					public boolean test() {
						//Si no esta dins del mapa mes petit.
						return (!mapaParets.contains(robot));
						}
					});
				//Si ens trobem als bordes, ens fiquem dins
			} else {
				setTurnRight(90);
				setAhead(100);
			}

			execute();
		}
	}

    /**
     * Executa totes les commandes per tal de disparar.
     * Primer calcula quin es el millor Cano
     * Segon dispara amb el millor Cano
     * Tercer dispara els canons falsos.
     */
public void disparar(){
		//Escollir la millor de totes
		long mesPunts = -1;
		Cano millorCano = null;
		for (Cano gun : canons) {
			if(gun.hits > mesPunts) {
				mesPunts = gun.hits;
				millorCano = gun;
			}
		}

		//Apuntar amb la millor de totes
		setTurnGunRightRadians(Utils.normalRelativeAngle(millorCano.getAngleDisparar(robot, target, POTENCIA) - getGunHeadingRadians()));

		//Si disparem, tambe disparem les bales falses.
		if(setFireBullet(POTENCIA) != null){
			for(Cano cano : canons) {
				Bala novaBala = new Bala();
				novaBala.setLocation(robot);
				novaBala.heading = cano.getAngleDisparar(robot, target, POTENCIA);
				//Robocode fisics velocitat bala.
				novaBala.velocity = 20.0 - 3.0 * POTENCIA;
				novaBala.canoUtilitzat = cano;
				bales.add(novaBala);
			}
		}
	}

    /**
     * override
     */
@Override
public void onScannedRobot(ScannedRobotEvent e){
	//Recrem un robot, posicio, velocitat, angle, i nom
	if (tancs.containsKey(e.getName())) {
		target = tancs.get(e.getName());
		target.setLocation(robot.calcularPunt(new Vector(e.getBearingRadians() + getHeadingRadians(), e.getDistance())));
		target.heading = e.getHeadingRadians();
		target.bearing = e.getBearing();
		target.velocity = e.getVelocity();
		target.name = e.getName();
		target.energy = e.getEnergy();
		
	}else {
		target = new Tanc();
		target.setLocation(robot.calcularPunt(new Vector(e.getBearingRadians() + getHeadingRadians(), e.getDistance())));
		target.heading = e.getHeadingRadians();
		target.bearing = e.getBearing();
		target.velocity = e.getVelocity();
		target.name = e.getName();
		target.energy = e.getEnergy();
		tancs.put(target.name, target);
	}
	for (String tanc : tancs.keySet()) {
		target = tancs.get(tanc);
		if(getDistanceRemaining()==0.0 && target.prevEnergy-e.getEnergy()>0.0){
			   setTurnRight(target.bearing + 90 - (10 * moveDirection));
			   setAhead(36*moveDirection);
			 }
		target.prevEnergy = e.getEnergy();
		
	}
	 //Si ens disparen, ens movem, sino quietets
	 
}

    /**
     * Override
     */
@Override
public void onCustomEvent(CustomEvent e) {
	if (e.getCondition().getName().equals("hulio")){
		removeCustomEvent(e.getCondition());
		// switch directions and move away
		moveDirection *= -1;
		setAhead(100 * moveDirection);
	}
}

    /**
     * Override
     */
@Override
public void onHitRobot(HitRobotEvent e) {
	moveDirection *= -11;
    setAhead(180 * moveDirection);
}

    /**
     * Override
     */
@Override
public void onBulletHit(BulletHitEvent e) {
	//Si li donem amb una bala, modifiquem el contador
	prevEnergy = e.getEnergy();
}

    /**
     * Override
     * No funciona, no se perque el onrobotdeath no es crida...
     * es bastant crucial ja que sino el radar es queda tieso
     */
@Override
public void onRobotDeath(RobotDeathEvent e){
	System.out.println(e.getName());
	tancs.remove(e.getName());
	target = null;
}

    /**
     * Implementacio contra cano en moviment amb targeting perfecte.
     */
public class CanoMoviment extends Cano {
	public double getAngleDisparar(Tanc shooter, Tanc target, double bulletPower) {
		double directAngle = shooter.absoluteAngleTo(target.calcularPunt(new Vector(target.heading, target.velocity/(20.0 - 3.0 * bulletPower))));
		return directAngle;
	}
}

    /**
     * Implementacio contra tanc que estigui quiet.
     */
public class CanoEstatic extends Cano {
	public double getAngleDisparar(Tanc shooter, Tanc target, double bulletPower) {
		return shooter.absoluteAngleTo(target);
	}
}

    /**
     * Implementacio contra cano en moviment amb targeting perfecte. Imp2
     */
public class CanoAsin extends Cano {
	public double getAngleDisparar(Tanc shooter, Tanc target, double bulletPower) {
		double directAngle = shooter.absoluteAngleTo(target);
		return directAngle - Math.asin(target.velocity * Math.sin(directAngle - target.heading) / (20.0 - 3.0 * bulletPower));
	}
}

    /**
     * Implementacio contra cano que segueixi el mateix tipo de moviment que nosaltres.
     */
public class CanoEsquiva extends Cano {
	public double getAngleDisparar(Tanc shooter, Tanc target, double bulletPower) {
		double directAngle = shooter.absoluteAngleTo(target.calcularPunt(new Vector(target.heading, 36)));
		return directAngle;
	}
}

    /**
     * Aquest es el +-Contracte de les implementacions cano.
     * Es static ja que implemento un metode generic de classe, per tal de reiniciar la puntuacio
     * dels canons quan canviem de target
     */
public static abstract class Cano {
	public long hits;

    /**
     * Funcio que retorna el angle amb el que s'ha de disprar la bala.
     * Ve a ser la inclinacio o heading del cano fals
     *
     * @param shooter Aquest parametre identifica el nostre tanc, el que dispara.
     * @param target Aquest parametre identifica el target, on hem de disparar.
     * @param bulletPower Aquest paramtre identifica la potencia que utilitzem.
     *
     * @return el angle amb el que ha de sortir la bala.
     */
	public abstract double getAngleDisparar(Tanc shooter, Tanc target, double bulletPower);

    /**
     * Funcio que reseteja el punts de tots els canons a una llista.
     * Util per si es un joc no-1vs1, ja que no donem prioritat a un cano quan tenim nou target
     *
     * @param canons llista de canons dels quals volem resetejar.
     */
	public static void ResetPunts(ArrayList<Cano> canons) {
		for(Cano cano : canons) {
			cano.hits = 0;
		}
	}
}

    /**
     * Classe que permet instanciar Punts del mapa.
     * Hereda de Point2D ja que permet utilitzar metodes utils.
     */
public class Punt extends Point2D.Double{
	/**
	 * Em molesten els warning aixi que...
	 */
	private static final long serialVersionUID = 1L;

	public Punt(){}
	public Punt(double x, double y){
		super(x,y);
	}

    /**
     * Funcio que permet calcular el angle entre dos punts.
     * Utilitzem el atan2 i no el atan, ja que permet mantenir el signe i identificar el quadrant.
     *
     * @param p Punt del qual volem saber el angle respecte a nosaltres.
     *
     * @return retorna el angle.
     */
	public double absoluteAngleTo(Punt p){
		double angle = Math.atan2(p.x - x, p.y - y);
		return Utils.normalAbsoluteAngle(angle);
	}

	//Com deia la meva profe de mates.. Punt = Punt + vector.
    /**
     * Funcio que serveix per calcular un nou Punt a partir del punt actual i un vector.
     *
     * @param v Vector que identifica fins a on volem desplaçar el punt.
     *
     * @return retorna el nou punt calculat.
     */
	public Punt calcularPunt(Vector v) {
		return new Punt(x + Math.sin(v.heading) * v.velocity, y + Math.cos(v.heading) * v.velocity);
	}
}

public class Vector extends Punt{
	/**
	 * Em molesten els warning aixi que...
	 */
	private static final long serialVersionUID = 1L;
	public double velocity, heading;
	public Vector() {}
	public Vector(double heading, double velocity) {
		this.heading = heading;
		this.velocity = velocity;
	}
}

public class Bala extends Vector{
	/**
	 * Em molesten els warning aixi que...
	 */
	private static final long serialVersionUID = 1L;
	public Cano canoUtilitzat;

    /**
     * Funcio que permet moure la bala falsa a cada tic.
     */
	public void moureBala() {
		setLocation(super.calcularPunt(this));
	}
}

public class Tanc extends Vector{
	/**
	 * Em molesten els warning aixi que...
	 */
	private static final long serialVersionUID = 1L;
	public String name;
	public double bearing,energy, prevEnergy;
}

    /**
     * Override
     *
     * Funcio que permet dibuixar per tal de debugear millor el robocode
     */
@Override
public void onPaint(Graphics2D g){
		g.setColor(Color.WHITE);
		//Servei per dibuixar les linies que fem servir de marge, el mapaParets
		g.drawRect(30,30,(int)getBattleFieldWidth()-60, (int)getBattleFieldHeight()-60);
}

}
